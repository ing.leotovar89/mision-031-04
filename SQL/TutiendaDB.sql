-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ciclo3demo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ciclo3demo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ciclo3demo` DEFAULT CHARACTER SET utf8 ;
USE `ciclo3demo` ;

-- -----------------------------------------------------
-- Table `ciclo3demo`.`nombreEmpresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`nombreEmpresa` (
  `idEmpresa` INT NOT NULL AUTO_INCREMENT,
  `NitEmpresa` INT NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Direccion` VARCHAR(45) NOT NULL,
  `Telefono` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEmpresa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`detalleCompra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`detalleCompra` (
  `idCompra` INT NOT NULL AUTO_INCREMENT,
  `idPedidoProductos` INT NOT NULL,
  `codigoProducto` VARCHAR(45) NOT NULL,
  `idClientes` INT NOT NULL,
  `codigoPedidos` INT NOT NULL,
  `cantidadProductos` FLOAT NOT NULL,
  `valorProductos` FLOAT NOT NULL,
  PRIMARY KEY (`idCompra`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`Categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`Categorias` (
  `codigoCategoria` INT NOT NULL AUTO_INCREMENT,
  `nombreCategoria` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`codigoCategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`Productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`Productos` (
  `idProductos` INT NOT NULL AUTO_INCREMENT,
  `nombreProducto` VARCHAR(45) NOT NULL,
  `cantidad` INT NOT NULL,
  `fechaVencimiento` VARCHAR(45) NOT NULL,
  `Descripcion` LONGTEXT NOT NULL,
  `codigoCategria` INT NOT NULL,
  `precioProductos` FLOAT NOT NULL,
  `Categorias_codigoCategoria` INT NOT NULL,
  `detalleCompra_idCompra` INT NOT NULL,
  `detalleCompra_idCompra1` INT NOT NULL,
  PRIMARY KEY (`idProductos`, `detalleCompra_idCompra`, `detalleCompra_idCompra1`),
  INDEX `fk_Productos_Categorias1_idx` (`Categorias_codigoCategoria` ASC) VISIBLE,
  INDEX `fk_Productos_detalleCompra1_idx` (`detalleCompra_idCompra1` ASC) VISIBLE,
  CONSTRAINT `fk_Productos_Categorias1`
    FOREIGN KEY (`Categorias_codigoCategoria`)
    REFERENCES `ciclo3demo`.`Categorias` (`codigoCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Productos_detalleCompra1`
    FOREIGN KEY (`detalleCompra_idCompra1`)
    REFERENCES `ciclo3demo`.`detalleCompra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`clientes` (
  `idclientes` INT NOT NULL AUTO_INCREMENT,
  `numeroDocumento` INT NOT NULL,
  `nombresCompleto` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idclientes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`Pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`Pedidos` (
  `codigoPedidos` INT NOT NULL AUTO_INCREMENT,
  `fechaDePedido` DATETIME(2) NOT NULL,
  `detalleCompra_idCompra` INT NOT NULL,
  PRIMARY KEY (`codigoPedidos`, `detalleCompra_idCompra`),
  INDEX `fk_Pedidos_detalleCompra1_idx` (`detalleCompra_idCompra` ASC) VISIBLE,
  CONSTRAINT `fk_Pedidos_detalleCompra1`
    FOREIGN KEY (`detalleCompra_idCompra`)
    REFERENCES `ciclo3demo`.`detalleCompra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`Inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`Inventario` (
  `idInventario` INT NOT NULL AUTO_INCREMENT,
  `nombreProducto` VARCHAR(45) NOT NULL,
  `excistenciaProducto` DOUBLE NOT NULL,
  `fechaCompra` VARCHAR(45) NOT NULL,
  `fechaEnvio` VARCHAR(45) NOT NULL,
  `precio` FLOAT NOT NULL,
  PRIMARY KEY (`idInventario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`ventas` (
  `Inventario_idInventario` INT NOT NULL AUTO_INCREMENT,
  `idventas` INT NOT NULL,
  `idcompra` INT NOT NULL,
  `idproducto` INT NOT NULL,
  `idCliente` INT NOT NULL,
  `cantidadProducto` DOUBLE NOT NULL,
  `valorProductos` FLOAT NOT NULL,
  `precioFinal` DOUBLE NOT NULL,
  PRIMARY KEY (`Inventario_idInventario`, `idventas`, `idproducto`, `idcompra`, `idCliente`),
  INDEX `fk_ventas_Inventario1_idx` (`Inventario_idInventario` ASC, `idcompra` ASC, `idproducto` ASC, `idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_ventas_Inventario1`
    FOREIGN KEY (`Inventario_idInventario` , `idcompra` , `idproducto` , `idCliente`)
    REFERENCES `ciclo3demo`.`Inventario` (`idInventario` , `idInventario` , `idInventario` , `idInventario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`ProductosAEmpresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`ProductosAEmpresa` (
  `idProductos` INT NOT NULL AUTO_INCREMENT,
  `idEmpresa` INT NOT NULL,
  `nombreProducto` VARCHAR(45) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`idProductos`, `idEmpresa`),
  INDEX `fk_Productos_has_nombreEmpresa_nombreEmpresa1_idx` (`idEmpresa` ASC) VISIBLE,
  INDEX `fk_Productos_has_nombreEmpresa_Productos1_idx` (`idProductos` ASC) VISIBLE,
  CONSTRAINT `fk_Productos_has_nombreEmpresa_Productos1`
    FOREIGN KEY (`idProductos`)
    REFERENCES `ciclo3demo`.`Productos` (`idProductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Productos_has_nombreEmpresa_nombreEmpresa1`
    FOREIGN KEY (`idEmpresa`)
    REFERENCES `ciclo3demo`.`nombreEmpresa` (`idEmpresa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`Productos_has_ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`Productos_has_ventas` (
  `Productos_idProductos` INT NOT NULL,
  `Productos_detalleCompra_idCompra` INT NOT NULL,
  `Productos_detalleCompra_idCompra1` INT NOT NULL,
  `ventas_idventas` INT NOT NULL,
  `ventas_Inventario_idInventario` INT NOT NULL,
  PRIMARY KEY (`Productos_idProductos`, `Productos_detalleCompra_idCompra`, `Productos_detalleCompra_idCompra1`, `ventas_idventas`, `ventas_Inventario_idInventario`),
  INDEX `fk_Productos_has_ventas_ventas1_idx` (`ventas_idventas` ASC, `ventas_Inventario_idInventario` ASC) VISIBLE,
  INDEX `fk_Productos_has_ventas_Productos1_idx` (`Productos_idProductos` ASC, `Productos_detalleCompra_idCompra` ASC, `Productos_detalleCompra_idCompra1` ASC) VISIBLE,
  CONSTRAINT `fk_Productos_has_ventas_Productos1`
    FOREIGN KEY (`Productos_idProductos` , `Productos_detalleCompra_idCompra` , `Productos_detalleCompra_idCompra1`)
    REFERENCES `ciclo3demo`.`Productos` (`idProductos` , `detalleCompra_idCompra` , `detalleCompra_idCompra1`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Productos_has_ventas_ventas1`
    FOREIGN KEY (`ventas_idventas` , `ventas_Inventario_idInventario`)
    REFERENCES `ciclo3demo`.`ventas` (`idventas` , `Inventario_idInventario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ciclo3demo`.`clientes_has_Pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ciclo3demo`.`clientes_has_Pedidos` (
  `clientes_idclientes` INT NOT NULL,
  `Pedidos_codigoPedidos` INT NOT NULL,
  `Pedidos_detalleCompra_idCompra` INT NOT NULL,
  PRIMARY KEY (`clientes_idclientes`, `Pedidos_codigoPedidos`, `Pedidos_detalleCompra_idCompra`),
  INDEX `fk_clientes_has_Pedidos_Pedidos1_idx` (`Pedidos_codigoPedidos` ASC, `Pedidos_detalleCompra_idCompra` ASC) VISIBLE,
  INDEX `fk_clientes_has_Pedidos_clientes1_idx` (`clientes_idclientes` ASC) VISIBLE,
  CONSTRAINT `fk_clientes_has_Pedidos_clientes1`
    FOREIGN KEY (`clientes_idclientes`)
    REFERENCES `ciclo3demo`.`clientes` (`idclientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_has_Pedidos_Pedidos1`
    FOREIGN KEY (`Pedidos_codigoPedidos` , `Pedidos_detalleCompra_idCompra`)
    REFERENCES `ciclo3demo`.`Pedidos` (`codigoPedidos` , `detalleCompra_idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
