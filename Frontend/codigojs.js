$(document).ready(function(){
    

    $('#btnAgregar').click(function(){
        let modal = new bootstrap.Modal('#exampleModal', { keyboard: false });

        //EVENTO CLICK DEL BOTON "Agregar Registro"
        $('#boton').unbind().click(function(){
            $.ajax({
            url: "http://localhost:8080/inventario/",
        //    url: "https://minticloud.uis.edu.co/c3s31grupo4/inventario/",
            data: JSON.stringify({ "idinvetario": $('#inputCodigo').val(), "nombreproducto": $('#inputProducto').val(), "excistenciaproducto": $('#inputexistencias').val(), "fechacompra": $('#inputFechaCompra').val(),"fechaenvio": $('#inputFechaEnvio').val(),"precio": $('#inputPrecio').val() }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "json",
            method: "POST",
            success: function(resultado){
                alert("Registro insertado correctamente.");
                window.location.reload();
            }
        });

    });

        modal.show();
    });

    //CODIGO PARA CONSULTAR Y GENERAR LA TABLA DE LOS "Apartamentos"
    $.ajax({
        url: "http://localhost:8080/inventario/",
      //  url: "https://minticloud.uis.edu.co/c3s31grupo4/inventario/",
        dataType: "json",
        method: "GET"
    }).done(function(registros){
        
        var datos = registros;
        $.each(datos, function(llave, fila){
            
            //CREA EL BOTON ELIMINAR JUNTO A SU EVENTO CLICK
            //this selecciona el elemento actual
            //parent selecciona el elemento padre o que contiene el elemento actual
            //find busca un elemento a partir de su tipo.
            var btnEliminar = $('<button type="button" class="btn btn-link">Eliminar</button>');
            btnEliminar.click(function(){
                $.ajax({
                    url: "http://localhost:8080/inventario/" + $(this).parent().parent().find("th").html(),

                  //  url: "https://minticloud.uis.edu.co/c3s31grupo4/inventario/" + $(this).parent().parent().find("th").html(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    type: "json",
                    method: "DELETE",
                    success: function(resultado){
                        alert("Se eliminó el registro correctamente.");
                        window.location.reload();
                    }
                });        
            });

            //CREA EL BOTON EDITAR JUNTO A SU EVENTO CLICK
            var btnEditar = $('<button type="button" class="btn btn-link">Editar</button>');
            btnEditar.click(function(){
                $.ajax({
                    url: "http://localhost:8080/inventario/" + $(this).parent().parent().find("th").html(),
                    //url: "https://minticloud.uis.edu.co/c3s31grupo4/inventario/" + $(this).parent().parent().find("th").html(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    type: "json",
                    method: "GET",
                    success: function(resultado){
                        //INSTANCIO EL MODAL QUE TIENE COMO ID "exampleModal"
                        let modal = new bootstrap.Modal('#exampleModal', { keyboard: false });

                        //ASIGNO LOS VALORES A CADA CAJA DE TEXTO
                        $('#inputCodigo').val(resultado.idinvetario);
                        $('#inputProducto').val(resultado.nombreproducto);
                        $('#inputexistencias').val(resultado.excistenciaproducto);
                        $('#inputFechaCompra').val(resultado.fechacompra);
                        $('#inputFechaEnvio').val(resultado.fechaenvio);
                        $('#inputPrecio').val(resultado.precio);

                        $('#boton').unbind().click(function(){
                            $.ajax({
                            url: "http://localhost:8080/inventario/",
                            //url: "https://minticloud.uis.edu.co/c3s31grupo4/inventario/",
                            data: JSON.stringify({ "idinvetario": $('#inputCodigo').val(), "nombreproducto": $('#inputProducto').val(), "excistenciaproducto": $('#inputexistencias').val(), "fechacompra": $('#inputFechaCompra').val(), "fechaenvio": $('#inputFechaEnvio').val(), "precio": $('#inputPrecio').val() }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            type: "json",
                            method: "PUT",
                            success: function(resultado){
                                alert("Registro actualizado correctamente.");
                                window.location.reload();
                            }
                        });
                        });

                        //MUESTRA EL MODAL
                        modal.show();
                
                    }
                });        


            });

            //CREA LA COLUMNA QUE SE AÑADIRA A LA FILA EXISTENTE.
            var columna = $('<td></td');
            columna.append(btnEditar);
            columna.append(btnEliminar);

            let filaBootstrap = "<tr>";
            filaBootstrap += "<th scope=\"row\">"+fila.idinvetario+"</th>";
            filaBootstrap += "<td>"+fila.nombreproducto+"</td>";
            filaBootstrap += "<td>"+fila.excistenciaproducto+"</td>";
            filaBootstrap += "<td>"+fila.fechacompra+"</td>";
            filaBootstrap += "<td>"+fila.fechaenvio+"</td>";
            filaBootstrap += "<td>"+fila.precio+"</td>";
            filaBootstrap += "</tr>";

            var fila = $(filaBootstrap).append(columna);

            $("#tblInventario tbody").append(fila);
        });
    });



    //EVENTO CLICK DEL BOTON "Eliminar"


});